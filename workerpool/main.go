package main

import (
	"fmt"
	"sync"
)

const (
	BadRequest  string = "bad"
	GoodRequest string = "good"
)

func main() {
	requests := make(chan map[string]int)
	// collect request
	go func() {
		for i := 0; i < 10000; i++ {
			if i%2 != 0 {
				requests <- map[string]int{
					BadRequest: i,
				}
			} else {
				requests <- map[string]int{
					GoodRequest: i,
				}
			}
		}
		close(requests)
	}()

	var wg sync.WaitGroup
	// check request
	wg.Add(5)
	for i := 0; i < 5; i++ {
		go func(worker int) {
			defer wg.Done()
			for request := range requests {
				for status, requestNumber := range request {
					fmt.Printf("worker %d check request %d successfully STATUS: %s request\n", worker, requestNumber, status)
				}
			}
		}(i)
	}
	wg.Wait()
}
