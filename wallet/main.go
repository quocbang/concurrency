package main

import (
	"fmt"
	"sync"
)

var (
	MyWallet int
	mutex    sync.Mutex
	wg       sync.WaitGroup
)

// default balance
func init() {
	MyWallet = 100
}

func Recharge(money int) {
	defer wg.Done()
	mutex.Lock()
	MyWallet += money
	mutex.Unlock()
}

func CashOut(money int) {
	defer wg.Done()
	mutex.Lock()
	MyWallet -= money
	mutex.Unlock()
}

func main() {
	wg.Add(3)
	go Recharge(20)
	go CashOut(50)
	go Recharge(30)
	wg.Wait()

	fmt.Println(MyWallet)
}
